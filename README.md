# Simple program demonstrating fuzzing for Linux on 64-bit Intel architecture
**Working on replacement of Peach with https://github.com/AFLplusplus/LibAFL/tree/main/fuzzers/forkserver_simple**

## Details
See the classroom exercise on Blackboard for instructions

## Reference
* JHU-ISI, Software Vulnerability Analysis, EN.650.660
* Reuben Johnston, reub@jhu.edu

## setup
* Install the dependencies
  * `$ sudo apt-get install mono-complete`
  * `$ cd ~/Downloads && wget https://sourceforge.net/projects/peachfuzz/files/Peach/3.1/peach-3.1.124-linux-x86_64-release.zip`
  * `$ sudo mkdir /opt/peach-3.1.124 && cd /opt/peach-3.1.124`
  * `$ sudo unzip ~/Downloads/peach-3.1.124-linux-x86_64-release.zip`
* Add the following alias to your ~/.bashrc
  * `alias peach="/opt/peach-3.1.124/peach"`
* Source the new bashrc
  * `$ source ~/.bashrc`
* Disable the firewall
  * `$ sudo ufw disable`
* Clone the repository into your sandbox
  * `$ git clone https://gitlab.com/underpantsgnomes/softwaresecurity/fuzzerlab.git`
* Import the project into eclipse and build vulnserver

## fuzzing
* Start the monitor (agent)
  * `$ peach -a tcp`
* From another terminal, start the fuzzer (publisher) 
  * `$ peach hterl.xml TestHTER`

## references
* Original vulnserver windows source here (http://sites.google.com/site/lupingreycorner/vulnserver.zip)
* Original vulnserver documented here (http://www.thegreycorner.com/p/vulnserver.html and http://www.thegreycorner.com/2010/12/introducing-vulnserver.html)
* https://sourceforge.net/projects/tinyhttpd
