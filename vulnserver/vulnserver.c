#define _WIN32_WINNT 0x501

/*
VulnServer - a deliberately vulnerable threaded TCP server application

This is vulnerable software, don't run it on an important system!  The author assumes no responsibility if
you run this software and your system gets compromised, because this software was designed to be exploited!

Ported by reub@jhu.edu to Linux for teaching purposes.  In the port, used code from tinyhttpd by J. David Blackstone.

*/

/*
Visit the authors blog for more details: http://grey-corner.blogspot.com
 */


/*
Copyright (c) 2010, Stephen Bradshaw
All rights reserved.

Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:

 * Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
 * Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.
 * Neither the name of the organization nor the names of its contributors may be used to endorse or promote products derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

/* J. David's webserver */
/* This is a simple webserver.
 * Created November 1999 by J. David Blackstone.
 * CSE 4344 (Network concepts), Prof. Zeigler
 * University of Texas at Arlington
 */


#include <stdio.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <unistd.h>
#include <ctype.h>
#include <strings.h>
#include <string.h>
#include <sys/stat.h>
#include <sys/wait.h>
#include <stdlib.h>

#define VERSION "1.00"
#define DEFAULT_BUFLEN 4096
#define DEFAULT_PORT "9999"

#define SERVER_STRING "Server: httpd/0.1.0\r\n"
#define ISspace(x) isspace((int)(x))

void Function1(char *Input);
void Function2(char *Input);
void Function3(char *Input);
void Function4(char *Input);

/**********************************************************************/
/* Print out an error message with perror() (for system errors; based
 * on value of errno, which indicates system call errors) and exit the
 * program indicating an error. */
/**********************************************************************/
void error_die(const char *sc)
{
	perror(sc);
	exit(1);
}

/**********************************************************************/
/* Get a line from a socket, whether the line ends in a newline,
 * carriage return, or a CRLF combination.  Terminates the string read
 * with a null character.  If no newline indicator is found before the
 * end of the buffer, the string is terminated with a null.  If any of
 * the above three line terminators is read, the last character of the
 * string will be a linefeed and the string will be terminated with a
 * null character.
 * Parameters: the socket descriptor
 *             the buffer to save the data in
 *             the size of the buffer
 * Returns: the number of bytes stored (excluding null) */
/**********************************************************************/
int get_line(int sock, char *buf, int size)
{
	int i = 0;
	char c = '\0';
	int n;

	while ((i < size - 1) && (c != '\n'))
	{
		n = recv(sock, &c, 1, 0);
		/* DEBUG printf("%02X\n", c); */
		if (n > 0)
		{
			if (c == '\r')
			{
				n = recv(sock, &c, 1, MSG_PEEK);
				/* DEBUG printf("%02X\n", c); */
				if ((n > 0) && (c == '\n'))
					recv(sock, &c, 1, 0);
				else
					c = '\n';
			}
			buf[i] = c;
			i++;
		}
		else
			c = '\n';
	}
	buf[i] = '\0';

	return(i);
}

/**********************************************************************/
/* Inform the client that the requested web method has not been
 * implemented.
 * Parameter: the client socket */
/**********************************************************************/
void unimplemented(int client)
{
	char buf[1024];

	sprintf(buf, "HTTP/1.0 501 Method Not Implemented\r\n");
	send(client, buf, strlen(buf), 0);
	sprintf(buf, SERVER_STRING);
	send(client, buf, strlen(buf), 0);
	sprintf(buf, "Content-Type: text/html\r\n");
	send(client, buf, strlen(buf), 0);
	sprintf(buf, "\r\n");
	send(client, buf, strlen(buf), 0);
	sprintf(buf, "<HTML><HEAD><TITLE>Method Not Implemented\r\n");
	send(client, buf, strlen(buf), 0);
	sprintf(buf, "</TITLE></HEAD>\r\n");
	send(client, buf, strlen(buf), 0);
	sprintf(buf, "<BODY><P>HTTP request method not supported.\r\n");
	send(client, buf, strlen(buf), 0);
	sprintf(buf, "</BODY></HTML>\r\n");
	send(client, buf, strlen(buf), 0);
}

/**********************************************************************/
/* Put the entire contents of a file out on a socket.  This function
 * is named after the UNIX "cat" command, because it might have been
 * easier just to do something like pipe, fork, and exec("cat").
 * Parameters: the client socket descriptor
 *             FILE pointer for the file to cat */
/**********************************************************************/
void cat(int client, FILE *resource)
{
	char buf[1024];

	fgets(buf, sizeof(buf), resource);
	while (!feof(resource))
	{
		send(client, buf, strlen(buf), 0);
		fgets(buf, sizeof(buf), resource);
	}
}

/**********************************************************************/
/* Return the informational HTTP headers about a file. */
/* Parameters: the socket to print the headers on
 *             the name of the file */
/**********************************************************************/
void headers(int client, const char *filename)
{
	char buf[1024];
	(void)filename;  /* could use filename to determine file type */

	strcpy(buf, "HTTP/1.0 200 OK\r\n");
	send(client, buf, strlen(buf), 0);
	strcpy(buf, SERVER_STRING);
	send(client, buf, strlen(buf), 0);
	sprintf(buf, "Content-Type: text/html\r\n");
	send(client, buf, strlen(buf), 0);
	strcpy(buf, "\r\n");
	send(client, buf, strlen(buf), 0);
}

/**********************************************************************/
/* Send a regular file to the client.  Use headers, and report
 * errors to client if they occur.
 * Parameters: a pointer to a file structure produced from the socket
 *              file descriptor
 *             the name of the file to serve */
/**********************************************************************/
void serve_file(int client, const char *filename)
{
	FILE *resource = NULL;
	int numchars = 1;
	char buf[1024];

	buf[0] = 'A'; buf[1] = '\0';
	while ((numchars > 0) && strcmp("\n", buf))  /* read & discard headers */
		numchars = get_line(client, buf, sizeof(buf));

	resource = fopen(filename, "r");
	if (resource == NULL)
		not_found(client);
	else
	{
		headers(client, filename);
		cat(client, resource);
	}
	fclose(resource);
}

/**********************************************************************/
/* Give a client a 404 not found status message. */
/**********************************************************************/
void not_found(int client)
{
	char buf[1024];

	sprintf(buf, "HTTP/1.0 404 NOT FOUND\r\n");
	send(client, buf, strlen(buf), 0);
	sprintf(buf, SERVER_STRING);
	send(client, buf, strlen(buf), 0);
	sprintf(buf, "Content-Type: text/html\r\n");
	send(client, buf, strlen(buf), 0);
	sprintf(buf, "\r\n");
	send(client, buf, strlen(buf), 0);
	sprintf(buf, "<HTML><TITLE>Not Found</TITLE>\r\n");
	send(client, buf, strlen(buf), 0);
	sprintf(buf, "<BODY><P>The server could not fulfill\r\n");
	send(client, buf, strlen(buf), 0);
	sprintf(buf, "your request because the resource specified\r\n");
	send(client, buf, strlen(buf), 0);
	sprintf(buf, "is unavailable or nonexistent.\r\n");
	send(client, buf, strlen(buf), 0);
	sprintf(buf, "</BODY></HTML>\r\n");
	send(client, buf, strlen(buf), 0);
}

/**********************************************************************/
/* A request has caused a call to accept() on the server port to
 * return.  Process the request appropriately.
 * Parameters: the socket connected to the client */
/**********************************************************************/
void accept_request(int client)
{
	char buf[1024];
	int numchars;
	char method[255];
	char url[255];
	char path[512];
	size_t i, j, k;
	struct stat st;
	char *query_string = NULL;

	char BigEmpty[1000];
	char *GdogBuf = malloc(1024);
	memset(BigEmpty, 0, 1000);

	numchars = get_line(client, buf, sizeof(buf));
	i = 0; j = 0;
	while (!ISspace(buf[j]) && (i < sizeof(method) - 1))
	{
		method[i] = buf[j];
		i++; j++;
	}
	method[i] = '\0';

	i = 0;
	while (ISspace(buf[j]) && (j < sizeof(buf)))
		j++;
	while (!ISspace(buf[j]) && (i < sizeof(url) - 1) && (j < sizeof(buf)))
	{
		url[i] = buf[j];
		i++; j++;
	}
	url[i] = '\0';

	if (strcasecmp(method, "GET") == 0)
	{
		query_string = url;
		while ((*query_string != '?') && (*query_string != '\0'))
			query_string++;
		if (*query_string == '?')
		{
			*query_string = '\0';
			query_string++;
		}

		sprintf(path, "htdocs%s", url);
		if (path[strlen(path) - 1] == '/')
			strcat(path, "index.html");
		if (stat(path, &st) == -1) {
			while ((numchars > 0) && strcmp("\n", buf))  /* read & discard headers */
				numchars = get_line(client, buf, sizeof(buf));
			not_found(client);
		}
		else
		{
			if ((st.st_mode & S_IFMT) == S_IFDIR)
				strcat(path, "/index.html");
			serve_file(client, path);
		}
	}
	else if (strcasecmp(method, "HELP") ==0 ) {
		const char ValidCommands[251] = "Valid Commands:\nHELP\nSTATS [stat_value]\nRTIME [rtime_value]\nLTIME [ltime_value]\nSRUN [srun_value]\nTRUN [trun_value]\nGMON [gmon_value]\nGDOG [gdog_value]\nKSTET [kstet_value]\nGTER [gter_value]\nHTER [hter_value]\nLTER [lter_value]\nKSTAN [lstan_value]\nEXIT\n";
		send( client, ValidCommands, sizeof(ValidCommands), 0 );
	}
	else if (strcasecmp(method, "STATS") ==0 ) {
		char *StatBuf = malloc(120);
		memset(StatBuf, 0, 120);
		strncpy(StatBuf, buf, 120);
		send( client, "STATS VALUE NORMAL\n", 19, 0 );
	}
	else if (strcasecmp(method, "RTIME") ==0 ) {
		char *RtimeBuf = malloc(120);
		memset(RtimeBuf, 0, 120);
		strncpy(RtimeBuf, buf, 120);
		send( client, "RTIME VALUE WITHIN LIMITS\n", 26, 0 );
	}
	else if (strcasecmp(method, "LTIME") ==0 ) {
		char *LtimeBuf = malloc(120);
		memset(LtimeBuf, 0, 120);
		strncpy(LtimeBuf, buf, 120);
		send( client, "LTIME VALUE HIGH, BUT OK\n", 25, 0 );
	}
	else if (strcasecmp(method, "SRUN") ==0 ) {
		char *SrunBuf = malloc(120);
		memset(SrunBuf, 0, 120);
		strncpy(SrunBuf, buf, 120);
		send( client, "SRUN COMPLETE\n", 14, 0 );
	}
	else if (strcasecmp(method, "TRUN") ==0 ) {
		char *TrunBuf = malloc(3000);
		memset(TrunBuf, 0, 3000);
		for (i = 5; i < sizeof(buf); i++) {
			if ((char)buf[i] == '.') {
				strncpy(TrunBuf, buf, 3000);
				Function3(TrunBuf);
				break;
			}
		}
		memset(TrunBuf, 0, 3000);
		send( client, "TRUN COMPLETE\n", 14, 0 );
	}
	else if (strcasecmp(method, "GMON") ==0 ) {
		char GmonStatus[13] = "GMON STARTED\n";
		for (i = 5; i < sizeof(buf); i++) {
			if ((char)buf[i] == '/') {
				if (strlen(buf) > 3950) {
					Function3(buf);
				}
				break;
			}
		}
		send( client, GmonStatus, sizeof(GmonStatus), 0 );
	}
	else if (strcasecmp(method, "GDOG") ==0 ) {
		strncpy(GdogBuf, buf, 1024);
		send( client, "GDOG RUNNING\n", 13, 0 );
	}
	else if (strcasecmp(method, "KSTET") ==0 ) {
		char *KstetBuf = malloc(100);
		strncpy(KstetBuf, buf, 100);
		memset(buf, 0, DEFAULT_BUFLEN);
		Function2(KstetBuf);
		send( client, "KSTET SUCCESSFUL\n", 17, 0 );
	}
	else if (strcasecmp(method, "GTER") == 0) {
		char *GterBuf = malloc(180);
		memset(GdogBuf, 0, 1024);
		strncpy(GterBuf, buf, 180);
		memset(buf, 0, DEFAULT_BUFLEN);
		Function1(GterBuf);
		send( client, "GTER ON TRACK\n", 14, 0 );
	}
	else if (strcasecmp(method, "HTER") ==0 ) {
		char THBuf[3];
		memset(THBuf, 0, 3);
		char *HterBuf = malloc((DEFAULT_BUFLEN+1)/2);
		memset(HterBuf, 0, (DEFAULT_BUFLEN+1)/2);
		i = 6;
		k = 0;
		while ( (buf[i]) && (buf[i+1])) {
			memcpy(THBuf, (char *)buf+i, 2);
			unsigned long j = strtoul((char *)THBuf, NULL, 16);
			memset((char *)HterBuf + k, (unsigned char)j, 1);
			i = i + 2;
			k++;
		}
		Function4(HterBuf);
		memset(HterBuf, 0, (DEFAULT_BUFLEN+1)/2);
		send( client, "HTER RUNNING FINE\n", 18, 0 );
	}
	else if (strcasecmp(method, "LTER") ==0 ) {
		char *LterBuf = malloc(DEFAULT_BUFLEN);
		memset(LterBuf, 0, DEFAULT_BUFLEN);
		i = 0;
		while(buf[i]) {
			if ((unsigned char)buf[i] > 0x7f) {
				LterBuf[i] = (unsigned char)buf[i] - 0x7f;
			} else {
				LterBuf[i] = buf[i];
			}
			i++;
		}
		for (i = 5; i < DEFAULT_BUFLEN; i++) {
			if ((char)LterBuf[i] == '.') {
				Function3(LterBuf);
				break;
			}
		}
		memset(LterBuf, 0, DEFAULT_BUFLEN);
		send( client, "LTER COMPLETE\n", 14, 0 );
	}
	else if (strcasecmp(method, "KSTAN") ==0 ) {
		send( client, "KSTAN UNDERWAY\n", 15, 0 );
	}
	else if (strcasecmp(method, "EXIT") ==0 ) {
		send( client, "GOODBYE\n", 8, 0 );
		printf("Connection closing...\n");
		close(client);
		exit(0);
	} else {
		send( client, "UNKNOWN COMMAND\n", 16, 0 );
	}
	close(client);
}

/**********************************************************************/
/* This function starts the process of listening for web connections
 * on a specified port.  If the port is 0, then dynamically allocate a
 * port and modify the original port variable to reflect the actual
 * port.
 * Parameters: pointer to variable containing the port to connect on
 * Returns: the socket */
/**********************************************************************/
int startup(u_short *port)
{
	int httpd = 0;
	struct sockaddr_in name;

	httpd = socket(PF_INET, SOCK_STREAM, 0);
	if (httpd == -1)
		error_die("socket");
	memset(&name, 0, sizeof(name));
	name.sin_family = AF_INET;
	name.sin_port = htons(*port);
	name.sin_addr.s_addr = htonl(INADDR_ANY);
	if (bind(httpd, (struct sockaddr *)&name, sizeof(name)) < 0)
		error_die("bind");
	if (*port == 0)  /* if dynamically allocating a port */
	{
		int namelen = sizeof(name);
		if (getsockname(httpd, (struct sockaddr *)&name, (socklen_t *)&namelen) == -1)
			error_die("getsockname");
		*port = ntohs(name.sin_port);
	}
	if (listen(httpd, 5) < 0)
		error_die("listen");
	return(httpd);
}

int main( int argc, char *argv[] ) {
	int server_sock = -1;
	u_short port = 45000;
	int client_sock = -1;
	struct sockaddr_in client_name;
	int client_name_len = sizeof(client_name);

	server_sock = startup(&port);
	printf("vulnserver httpd version %s running on port %d\n", VERSION, port);
	printf("\nThis is vulnerable software!\nDo not allow access from untrusted systems or networks!\n\n");

	EssentialFunc1();

	while (1)
	{
		client_sock = accept(server_sock,
				(struct sockaddr *)&client_name,
				(socklen_t *)&client_name_len);
		if (client_sock == -1)
			error_die("accept");
		accept_request(client_sock);
	}

	close(server_sock);

	return(0);
}


void Function1(char *Input) {
	char Buffer2S[140];
	strcpy(Buffer2S, Input);
}

void Function2(char *Input) {
	char Buffer2S[60];
	strcpy(Buffer2S, Input);
}

void Function3(char *Input) {
	char Buffer2S[2000];
	strcpy(Buffer2S, Input);
}

void Function4(char *Input) {
	char Buffer2S[1000];
	strcpy(Buffer2S, Input);
}

